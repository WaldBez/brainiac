package webcontroller.controllers;

import models.DocumentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import webcontroller.dao.KeywordDao;
import webcontroller.db.Keyword;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wald_Laptop on 9/25/2017.
 */
@RestController
@CrossOrigin
public class KeywordController {

    @Autowired
    private KeywordDao keywordDao;

    private final RestTemplate restTemplate = new RestTemplate();

    @RequestMapping("/keywords/saveKeywords")
    public String saveKeywords(@RequestBody final Keyword[] keywords) {
        keywordDao.saveKeywords(keywords);
        return "Keywords saved";
    }

    @RequestMapping("/keywords/fetchAllKeywords")
    public List<Keyword> fetchAllKeywords() {
        List<Keyword> keywords = keywordDao.findAllKeyword();
        for (final Keyword keyword : keywords) {
            keyword.setClassifications(null);
            keyword.setRootWords(null);
        }
        return keywords;
    }

    @RequestMapping("/keywords/retrainSOM")
    public String retrainSOM() {
        restTemplate.getForObject("http://localhost:8002/retrainSOM", Boolean.class);
        return "Retrained";
    }

    @RequestMapping("/keywords/deleteKeyword")
    public String deleteKeyword(@RequestBody final Keyword keyword) {
        keywordDao.deleteKeyword(keyword);
        return "Deleted";
    }

    @RequestMapping("/keywords/saveNewDomain")
    public String saveNewDomain(@RequestBody final DocumentModel documentModel) {
        keywordDao.deleteAll();
        keywordDao.saveKeywords(constructFromDocument(documentModel));
        return "New Domain Saved";
    }

    private Keyword[] constructFromDocument(final DocumentModel documentModel) {
        final ArrayList<Keyword> keywords = new ArrayList();
        for (final String word : documentModel.getFileAsText().split("\r\n")) {
            keywords.add(new Keyword(word, false));
        }
        return keywords.toArray(new Keyword[keywords.size()]);
    }
}
