package webcontroller.controllers;

import models.DocumentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import webcontroller.dao.DocumentDao;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by Wald_Laptop on 9/5/2017.
 */
@RestController
@CrossOrigin
public class FileController {

    @Autowired
    private DocumentDao documentDao;

    @RequestMapping("/file/sendFilesToBackend")
    public String receiveFiles(@RequestBody final DocumentModel[] documentModels) {
        try {
            documentDao.saveModelDocuments(documentModels);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Documents Saved";
    }

    @RequestMapping("/file/fetchAllDocuments")
    public List<DocumentModel> fetchAllDocuments() {
        return documentDao.fetchAllDocuments();
    }

    @RequestMapping("/file/deleteDocuments")
    public String deleteDocuments(@RequestBody final DocumentModel documentModels) {
        try {
            documentDao.deleteDocument(documentModels);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Deleted";
    }
}
