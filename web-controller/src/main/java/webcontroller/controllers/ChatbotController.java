package webcontroller.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Wald_Laptop on 10/29/2017.
 */
@RestController
@CrossOrigin
public class ChatbotController {

    private final RestTemplate restTemplate = new RestTemplate();

    @RequestMapping("/chatbot/talk")
    public String talkToBot(@RequestBody final String question) {
        final String message = restTemplate.postForObject("http://localhost:8003/lets/talk", question, String.class);
        return message;
    }
}
