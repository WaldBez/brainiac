package webcontroller.controllers;

import models.DocumentModel;
import models.Matrix;
import models.Page;
import models.Tuple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import webcontroller.dao.DocumentDao;
import webcontroller.dao.KeywordDao;
import webcontroller.mappers.MapToPage;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Wald_Laptop on 8/15/2017.
 */
@RestController
@CrossOrigin
public class AnalysisController {

    @Autowired
    private DocumentDao documentDao;

    @Autowired
    private KeywordDao keywordDao;

    private final RestTemplate restTemplate = new RestTemplate();
    private Matrix matrix;
    private ArrayList<Tuple> map;

    @RequestMapping("/")
    public String home() {
        return "Test";
    }

    @RequestMapping("/analysis/sendToLSI")
    public Matrix sendToLSI() {
        matrix = restTemplate.postForObject("http://localhost:8001/RunLSI", createDocuments(), Matrix.class);
        matrix.convertHashMapForEachRow();
        return matrix;
    }

    @RequestMapping("/analysis/sendToSOM")
    public ArrayList<Tuple> sendToSOM() {
        map = new ArrayList<Tuple>(Arrays.asList(restTemplate.postForObject("http://localhost:8002/RunMatrixInSOM", matrix, Tuple[].class)));
        return map;
    }

    @RequestMapping("/analysis/runFullAnalysis")
    public ArrayList<Page> runFullAnalysis() {
        matrix = restTemplate.postForObject("http://localhost:8001/RunLSI", documentDao.fetchAllDocuments(), Matrix.class);
        matrix.convertHashMapForEachRow();
        map = new ArrayList<Tuple>(Arrays.asList(restTemplate.postForObject("http://localhost:8002/RunMatrixInSOM", matrix, Tuple[].class)));
        return new MapToPage().mapMatrixAndSomToPages(matrix, map, keywordDao.findAllKeyword());
    }

    @RequestMapping("/analysis/fetchUMatrix")
    public Double[][] fetchUMatrix(@RequestBody final String term) {
        return restTemplate.postForObject("http://localhost:8002/getUMatrixForTerm", term, Double[][].class);
    }

    @RequestMapping("/analysis/fetchSomDistanceAtDifferentTrainingPoints")
    public Integer[] fetchSomDistanceAtDifferentTrainingPoints(@RequestBody final String term) {
        return restTemplate.postForObject("http://localhost:8002/getSomDistanceAtDifferentTrainingPoints", term, Integer[].class);
    }

    private ArrayList<DocumentModel> createDocuments() {
        final ArrayList<DocumentModel> documentModels = new ArrayList<DocumentModel>();
        final DocumentModel documentModel1 = new DocumentModel(1, "Computer DocumentModel 1", new File(getClass().getResource("../../Documents/Computers_1.txt").getPath()));
        final DocumentModel documentModel2 = new DocumentModel(2, "Computer DocumentModel 2", new File(getClass().getResource("../../Documents/Computers_2.txt").getPath()));
        documentModels.add(documentModel1);
        documentModels.add(documentModel2);
        return documentModels;
    }
}
