package webcontroller.dao;

import models.DocumentModel;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import webcontroller.AbstractDAO;
import webcontroller.db.Document;
import webcontroller.db.User;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wald_Laptop on 9/24/2017.
 */
@Repository
@Transactional
public class DocumentDao extends AbstractDAO {

    public boolean saveModelDocuments(final DocumentModel[] documentModels) throws UnsupportedEncodingException {
        final User user = new User();
        user.setUserId(1);
        for (int a = 0; a < documentModels.length; a++) {
            if (!documentModels[a].isAnalyse()) {
                final byte[] textAsBytes = documentModels[a].getFileAsText().getBytes("UTF-8");
                saveDocument(new Document(user, documentModels[a].getName(), textAsBytes, false));
            }
        }
        return true;
    }

    public List<DocumentModel> fetchAllDocuments() {
        final List<DocumentModel> documentModels = new ArrayList();
        final List<Document> dbDocuments = findAllDocuments();
        for (final Document dbDocument : dbDocuments) {
            final byte[] blob = dbDocument.getFileBlob();
            documentModels.add(new DocumentModel(dbDocument.getDocumentId(), dbDocument.getDocumentName(), new String(blob)));
        }
        return documentModels;
    }

    public void saveDocument(final Document document) {
        getSession().persist(document);
    }

    public boolean deleteDocument(final DocumentModel document) throws UnsupportedEncodingException {
        final User user = new User();
        user.setUserId(1);
        final byte[] textAsBytes = document.getFileAsText().getBytes("UTF-8");
        final Document dbDocument = new Document(user, document.getName(), textAsBytes, false);
        dbDocument.setDocumentId(document.getId());
        getSession().delete(dbDocument);
        return true;
    }

    @SuppressWarnings("unchecked")
    public List<webcontroller.db.Document> findAllDocuments() {
        Criteria criteria = getSession().createCriteria(Document.class);
        return (List<Document>) criteria.list();
    }
}
