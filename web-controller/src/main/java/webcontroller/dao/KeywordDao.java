package webcontroller.dao;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import webcontroller.AbstractDAO;
import webcontroller.db.Keyword;

import java.util.List;

/**
 * Created by Wald_Laptop on 9/25/2017.
 */
@Repository
@Transactional
public class KeywordDao extends AbstractDAO {

    public boolean saveKeywords(final Keyword[] keywords) {
        for (final Keyword keyword : keywords) {
            if (!keyword.isAnalyseWord()) {
                keyword.setAnalyseWord(true);
                saveKeyword(keyword);
            }
        }
        return true;
    }

    public void saveKeyword(final Keyword keyword) {
        getSession().persist(keyword);
    }

    public boolean deleteKeyword(final Keyword keyword) {
        getSession().delete(keyword);
        return true;
    }

    public boolean deleteAll() {
        final List<Keyword> allWords = findAllKeyword();
        for (final Keyword keyword : allWords) {
            getSession().delete(keyword);
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    public List<Keyword> findAllKeyword() {
        Criteria criteria = getSession().createCriteria(Keyword.class);
        return (List<Keyword>) criteria.list();
    }
}
