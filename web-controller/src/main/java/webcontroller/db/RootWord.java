package webcontroller.db;
// Generated Sep 25, 2017 4:17:04 PM by Hibernate Tools 3.2.2.GA


import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * RootWord generated by hbm2java
 */
@Entity
@Table(name="root_word"
    ,catalog="brainiac"
)
public class RootWord  implements java.io.Serializable {


     private Integer rootWordId;
     private Keyword keyword;
     private String rootWord;
     private Set<Classification> classifications = new HashSet<Classification>(0);

    public RootWord() {
    }

	
    public RootWord(Keyword keyword, String rootWord) {
        this.keyword = keyword;
        this.rootWord = rootWord;
    }
    public RootWord(Keyword keyword, String rootWord, Set<Classification> classifications) {
       this.keyword = keyword;
       this.rootWord = rootWord;
       this.classifications = classifications;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="ROOT_WORD_ID", unique=true, nullable=false)
    public Integer getRootWordId() {
        return this.rootWordId;
    }
    
    public void setRootWordId(Integer rootWordId) {
        this.rootWordId = rootWordId;
    }
@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="KEYWORD_ID", nullable=false)
    public Keyword getKeyword() {
        return this.keyword;
    }
    
    public void setKeyword(Keyword keyword) {
        this.keyword = keyword;
    }
    
    @Column(name="ROOT_WORD", nullable=false)
    public String getRootWord() {
        return this.rootWord;
    }
    
    public void setRootWord(String rootWord) {
        this.rootWord = rootWord;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="rootWord")
    public Set<Classification> getClassifications() {
        return this.classifications;
    }
    
    public void setClassifications(Set<Classification> classifications) {
        this.classifications = classifications;
    }




}


