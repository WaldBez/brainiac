package webcontroller.mappers;

import models.*;
import webcontroller.db.Keyword;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Wald_Laptop on 8/28/2017.
 */
public class MapToPage {

    public ArrayList<Page> mapMatrixAndSomToPages(final Matrix matrix, final ArrayList<Tuple> somMap, final List<Keyword> keywords) {
        final ArrayList<Page> pages = new ArrayList();
        for (final TextDocumentTuple paragraph : matrix.getParagraphs()) {
            if (!paragraph.getText().equals("\n")) {
                findPageForEachRow(matrix, somMap, pages, paragraph, keywords);
            }
        }
        return mergePages(pages);
    }

    private ArrayList<Page> mergePages(final ArrayList<Page> pages) {
        final ArrayList<Page> mergedPages = new ArrayList();
        boolean pageAlreadyAdded = false;
        for (final Page page : pages) {
            for (final Page mergedPage : mergedPages) {
                if (page.getTitle().equals(mergedPage.getTitle())) {
                    mergedPage.getParagraph().add(page.getParagraph().get(0));
                    pageAlreadyAdded = true;
                }
            }
            if (!pageAlreadyAdded)
                mergedPages.add(page);
            pageAlreadyAdded = false;
        }
        return mergedPages;
    }

    private void findPageForEachRow(final Matrix matrix, final ArrayList<Tuple> somMap, final ArrayList<Page> pages, final TextDocumentTuple paragraph, final List<Keyword> keywords) {
        final List<String> keywordsAsStrings = mapKeywordsToString(keywords);
        for (final MatrixRow row : matrix.getRows()) {
            if (row.getDocumentNameAsId().equals(paragraph.getDocument())) {
                final Page page = new Page();
                page.setSomTitle(findBestMatchingWord(row, somMap, page));
                page.addParagraph(paragraph.getText());
                page.setActualTile(keywordsAsStrings);
                pages.add(page);
            }
        }
    }

    private List<String> mapKeywordsToString(final List<Keyword> keywords) {
        final List<String> keywordsAsStrings = new ArrayList();
        for (final Keyword keyword : keywords) {
            keywordsAsStrings.add(keyword.getKeywordValue());
        }
        return keywordsAsStrings;
    }

    private String findBestMatchingWord(final MatrixRow row, final ArrayList<Tuple> somMap, final Page page) {
        final HashMap<String, Integer> mappingsCount = new HashMap();
        for (final Tuple somTuple : somMap) {
            for (final Term term : row.getTerms()) {
                if (term.getTerm().equals(somTuple.getTerm())) {
                    if (mappingsCount.get(somTuple.getMappedValue()) != null)
                        mappingsCount.put(somTuple.getMappedValue(), mappingsCount.get(somTuple.getMappedValue()).intValue() + term.getCount());
                    else {
                        mappingsCount.put(somTuple.getMappedValue(), term.getCount());
                    }
                }
            }
        }
        return wordWithHighestCount(mappingsCount);
    }

    private String wordWithHighestCount(final HashMap<String, Integer> mappingsCount) {
        final Iterator iterator = mappingsCount.entrySet().iterator();
        HashMap.Entry bestPair = (HashMap.Entry)iterator.next();
        while (iterator.hasNext()) {
            final HashMap.Entry nextPair = (HashMap.Entry)iterator.next();
            if ((Integer)bestPair.getValue() < (Integer)nextPair.getValue())
                bestPair = nextPair;
        }
        return (String) bestPair.getKey();
    }
}
