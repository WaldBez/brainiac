package webcontroller;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Wald_Laptop on 9/23/2017.
 */
public abstract class AbstractDAO {

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void persist(final Object entity) {
        getSession().persist(entity);
    }

    public void delete(final Object entity) {
        getSession().delete(entity);
    }
}
