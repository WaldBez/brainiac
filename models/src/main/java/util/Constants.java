package util;

/**
 * Created by Wald on 3/14/2017.
 */
public class Constants {

    public static final String ADJECTIVES_FILE = "/words-to-remove/adjectives.txt";
    public static final String ARTICLES_FILE = "/words-to-remove/articles.txt";
    public static final String CONJUNCTIONS_FILE = "/words-to-remove/conjunctions.txt";
    public static final String FRILLY_WORDS_FILE = "/words-to-remove/frilly-words.txt";
    public static final String PREPOSITIONS_FILE = "/words-to-remove/prepositions.txt";
    public static final String PRONOUNS_FILE = "/words-to-remove/pronouns.txt";
    public static final String SINGLE_LETTERS_FILE = "/words-to-remove/single-letters.txt";
    public static final String VERBS_FILE = "/words-to-remove/verbs.txt";
    public static final String SOM_TRAINING_WORDS = "/documents/training-terms.txt";

    public static final int ASCII_LOWER_BOUND = 65;
    public static final int ASCII_UPPER_BOUND = 90;
    public static final int WEIGHTS_LENGTH = 50;
    public static final int NEURONS_DIMENSION = 20;

    public static final double SOM_TRAINING_ITERATION_FACTOR = 0.00001;
}
