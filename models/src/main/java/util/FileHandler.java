package util;

import models.DocumentModel;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Wald on 3/14/2017.
 */
public class FileHandler {

    public ArrayList<String> readWordsFromFile(final String fileName) throws IOException {
        final InputStream fis = getClass().getResourceAsStream(fileName);
        final BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        ArrayList<String> wordsFromFile = new ArrayList<String>();
        String currentLine;
        while ((currentLine = br.readLine()) != null) {
            wordsFromFile.add(currentLine);
        }
        return wordsFromFile;
    }

    public BufferedReader createBufferedReaderForFile(final DocumentModel documentModel) throws FileNotFoundException {
        final InputStream fis = new FileInputStream(documentModel.getFile());
        return new BufferedReader(new InputStreamReader(fis));
    }

    public ArrayList<String> createStringOfAllLinesInFile(final BufferedReader reader) throws IOException {
        final ArrayList<String> paragraphs = new ArrayList<String>();
        StringBuilder builder = new StringBuilder();
        String currentLine = reader.readLine();
        while (currentLine != null) {
            builder.append(currentLine).append("\n");
            currentLine = reader.readLine();
            builder = assignNewParagraphToBuilder(paragraphs, builder, currentLine);
        }
        paragraphs.add(builder.toString());
        return paragraphs;
    }

    private StringBuilder assignNewParagraphToBuilder(final ArrayList<String> paragraphs, final StringBuilder builder, final String currentLine) {
        if (currentLine != null && currentLine.equals("")) {
            paragraphs.add(builder.toString());
            return new StringBuilder();
        }
        return builder;
    }
}
