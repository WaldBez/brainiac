package models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Wald_Laptop on 6/4/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Term {

    private String term;
    private int count;

    public Term() {}

    public Term(final String term, final int count) {
        this.term = term;
        this.count = count;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(final String term) {
        this.term = term;
    }

    public int getCount() {
        return count;
    }

    public void setCount(final int count) {
        this.count = count;
    }
}
