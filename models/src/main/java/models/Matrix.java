package models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by Wald_Laptop on 6/3/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Matrix {

    private ArrayList<MatrixRow> rows = new ArrayList();
    ArrayList<TextDocumentTuple> paragraphs = new ArrayList();

    public Matrix() {}

    public ArrayList<MatrixRow> getRows() {
        return rows;
    }

    public void setRows(final ArrayList<MatrixRow> rows) {
        this.rows = rows;
    }

    public ArrayList<TextDocumentTuple> getParagraphs() {
        return paragraphs;
    }

    public void setParagraphs(final ArrayList<TextDocumentTuple> paragraphs) {
        this.paragraphs = paragraphs;
    }

    public void convertHashMapForEachRow() {
        for (final MatrixRow row : rows) {
            row.convertHashMapToArraylist();
        }
    }
}
