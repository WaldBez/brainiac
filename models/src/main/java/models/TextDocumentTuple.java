package models;

import java.util.ArrayList;

/**
 * Created by Wald_Laptop on 8/28/2017.
 */
public class TextDocumentTuple {

    private String document;
    private String text;
    private ArrayList<String> textAsWords = new ArrayList<String>();

    public TextDocumentTuple() {}

    public String getDocument() {
        return document;
    }

    public void setDocument(final String document) {
        this.document = document;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public ArrayList<String> getTextAsWords() {
        return textAsWords;
    }

    public void setTextAsWords(final ArrayList<String> textAsWords) {
        this.textAsWords = textAsWords;
    }
}
