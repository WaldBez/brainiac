package models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Wald_Laptop on 6/3/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatrixRow {

    private String documentNameAsId;
    private HashMap<String, Integer> termsCount = new HashMap<String, Integer>();
    private ArrayList<Term> terms = new ArrayList<Term>();

    public MatrixRow() {}

    public String getDocumentNameAsId() {
        return documentNameAsId;
    }

    public void setDocumentNameAsId(final String documentNameAsId) {
        this.documentNameAsId = documentNameAsId;
    }

    public HashMap<String, Integer> getTermsCount() {
        return termsCount;
    }

    public void setTermsCount(final HashMap<String, Integer> termsCount) {
        this.termsCount = termsCount;
    }

    public ArrayList<Term> getTerms() {
        return terms;
    }

    public void setTerms(final ArrayList<Term> terms) {
        this.terms = terms;
    }

    public void convertHashMapToArraylist() {
        final Iterator iterator = this.termsCount.entrySet().iterator();
        while (iterator.hasNext()) {
            final HashMap.Entry pair = (HashMap.Entry)iterator.next();
            this.terms.add(new Term((String) pair.getKey(), (Integer) pair.getValue()));
        }
    }
}
