package models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wald_Laptop on 8/28/2017.
 */
public class Page {

    private String title;
    private String somTitle;
    private ArrayList<String> paragraph = new ArrayList();
    private ArrayList<String> possibleWords = new ArrayList();
    private ArrayList<Page> pages = new ArrayList();

    public Page() {}

    public Page(final String somTitle, final String paragraph) {
        this.somTitle = somTitle;
        this.paragraph.add(paragraph);
    }

    public void setSomTitle(String somTitle) {
        this.somTitle = somTitle;
    }

    public void setActualTile(final List<String> keywords) {
        int wordDistance = this.determineDistance(keywords.get(0).toUpperCase().toCharArray(),
                this.somTitle.toUpperCase().toCharArray());
        int index = 0;
        for (int a = 0; a < keywords.size(); a++) {
            int currentDistance = this.determineDistance(keywords.get(a).toUpperCase().toCharArray(),
                    this.somTitle.toUpperCase().toCharArray());
            if (wordDistance < currentDistance) {
                wordDistance = currentDistance;
                index = a;
            }
        }
        this.title = keywords.get(index);
    }

    public void addParagraph(final String text) {
        this.paragraph.add(text);
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<String> getParagraph() {
        return paragraph;
    }

    public void setParagraph(final ArrayList<String> paragraph) {
        this.paragraph = paragraph;
    }

    public ArrayList<Page> getPages() {
        return pages;
    }

    public void setPages(final ArrayList<Page> pages) {
        this.pages = pages;
    }

    private int determineDistance(final char[] word, final char[] somWordAsArray) {
        final double minLenght = word.length < somWordAsArray.length ? word.length : somWordAsArray.length;
        int similarityCount = 0;
        for (int a = 0; a < minLenght; a++) {
            if (word[a] == somWordAsArray[a]) {
                similarityCount += 1;
            }
        }
        return similarityCount;
    }
}
