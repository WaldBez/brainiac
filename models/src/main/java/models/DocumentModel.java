package models;

import java.io.File;

/**
 * Created by Wald_Laptop on 6/3/2017.
 */
public class DocumentModel {

    private Integer id;

    private String name;

    private File file;

    private String fileAsText;

    private boolean analyse;

    public DocumentModel() {}

    public DocumentModel(final Integer id, final String name) {
        this.id = id;
        this.name = name;
        this.analyse = true;
    }

    public DocumentModel(final Integer id, final String name, final File file) {
        this.id = id;
        this.name = name;
        this.file = file;
        this.analyse = true;
    }

    public DocumentModel(final Integer id, final String name, final String fileAsText) {
        this.id = id;
        this.name = name;
        this.fileAsText = fileAsText;
        this.analyse = true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public File getFile() {
        return file;
    }

    public void setFile(final File file) {
        this.file = file;
    }

    public String getFileAsText() {
        return fileAsText;
    }

    public void setFileAsText(final String fileAsText) {
        this.fileAsText = fileAsText;
    }

    public boolean isAnalyse() {
        return analyse;
    }

    public void setAnalyse(final boolean analyse) {
        this.analyse = analyse;
    }
}
