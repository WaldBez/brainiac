package models;

/**
 * Created by Wald_Laptop on 8/15/2017.
 */
public class Tuple {

    private String term;
    private String mappedValue;

    public Tuple() {}

    public Tuple(final String term, final String mappedValue) {
        this.term = term;
        this.mappedValue = mappedValue;
    }

    public String getTerm() {
        return term;
    }

    public String getMappedValue() {
        return mappedValue;
    }
}
