package som;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Wald_Laptop on 9/25/2017.
 */
public class AbstractDao {

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void persist(final Object entity) {
        getSession().persist(entity);
    }

    public void delete(final Object entity) {
        getSession().delete(entity);
    }
}
