package som.dao;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import som.AbstractDao;
import som.db.Keyword;

import java.util.List;

/**
 * Created by Wald_Laptop on 9/25/2017.
 */
@Repository
@Transactional
public class KeywordDao extends AbstractDao {

    @SuppressWarnings("unchecked")
    public List<Keyword> findAllKeyword() {
        Criteria criteria = getSession().createCriteria(Keyword.class);
        return (List<Keyword>) criteria.list();
    }
}
