package som;

import models.Matrix;
import models.Tuple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import som.dao.KeywordDao;
import som.domain.Node;
import som.domain.SelfOrganisingMap;
import som.domain.TermToSOMMapper;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

/**
 * Created by Wald_Laptop on 8/6/2017.
 */
@RestController
@Component
public class Server {

    public static Set<SelfOrganisingMap> somAtDifferentIntervals = new HashSet<SelfOrganisingMap>();

    @Autowired
    private KeywordDao keywordDao;

    private SelfOrganisingMap som = new SelfOrganisingMap();

    @PostConstruct
    public void initialize() {
        try {
            som.initializeSOM(keywordDao);
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/")
    public String home() throws IOException {
        return "Welcome To Self Organising Map.";
    }

    @RequestMapping(value = "RunMatrixInSOM", method = RequestMethod.POST)
    public ArrayList<Tuple> runMatrixInSOM(@RequestBody final Matrix matrix) throws IOException {
        final TermToSOMMapper mapper = new TermToSOMMapper();
        return convertHashMapToArraylist(mapper.mapTermsFromMatrixOnSOM(matrix, som));
    }

    @RequestMapping(value = "retrainSOM", method = RequestMethod.GET)
    public Boolean retrainSOM() {
        Server.somAtDifferentIntervals = new HashSet<SelfOrganisingMap>();
        som = new SelfOrganisingMap();
        try {
            som.initializeSOM(keywordDao);
            return true;
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @RequestMapping(value = "getUMatrixForTerm", method = RequestMethod.POST)
    public Double[][] getUMatrixForTerm(@RequestBody final String term) {
        return new TermToSOMMapper().mapToUMatrix(term, som);
    }

    @RequestMapping(value = "getSomDistanceAtDifferentTrainingPoints", method = RequestMethod.POST)
    public Integer[] getSomDistanceAtDifferentIntervals(@RequestBody final String term) {
        return new TermToSOMMapper().mapToSomAtInterval(term, Server.somAtDifferentIntervals);
    }

    private ArrayList<Tuple> convertHashMapToArraylist(final HashMap<String, String> map) {
        final ArrayList<Tuple> tuples = new ArrayList<Tuple>();
        final Iterator iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            final HashMap.Entry pair = (HashMap.Entry)iterator.next();
            tuples.add(new Tuple((String) pair.getKey(), (String) pair.getValue()));
        }
        return tuples;
    }
}
