package som.domain;

import models.Matrix;
import models.MatrixRow;
import models.Term;
import models.Tuple;
import util.Constants;

import java.util.*;

/**
 * Created by Wald_Laptop on 8/15/2017.
 */
public class TermToSOMMapper {

    public HashMap<String, String> mapTermsFromMatrixOnSOM(final Matrix matrix, final SelfOrganisingMap som) {
        final HashMap<String, String> map = new HashMap<String, String>();
        for (final MatrixRow row : matrix.getRows()) {
            for (final Term term : row.getTerms()) {
                final Tuple tuple = mapTermToSOM(term, som);
                map.put(tuple.getTerm(), tuple.getMappedValue());
            }
        }
        return map;
    }

    public Double[][] mapToUMatrix(final String term, final SelfOrganisingMap som) {
        final Double[][] termMatrix = new Double[Constants.NEURONS_DIMENSION][Constants.NEURONS_DIMENSION];
        for (int a = 0; a < som.getSomNetwork().length; a++) {
            for (int b = 0; b < som.getSomNetwork()[a].length; b++) {
                termMatrix[a][b] = som.getSomNetwork()[a][b].determineDistance(convertWordToIntArray(term));
            }
        }
        return termMatrix;
    }

    public Integer[] mapToSomAtInterval(final String term, final Set<SelfOrganisingMap> som) {
        final Integer[] somAtDifferentIntervals = new Integer[som.size()];
        int count = 0;
        for (final SelfOrganisingMap somNode : sortedSOMList(som)) {
            somAtDifferentIntervals[count] = findBestMatchingInNodes(mapToUMatrixForNode2DArray(term, somNode.getSavedSomNetwork()));
            count++;
        }
        return somAtDifferentIntervals;
    }

    private ArrayList<SelfOrganisingMap> sortedSOMList(final Set<SelfOrganisingMap> soms) {
        final Set<SelfOrganisingMap> localSom = new HashSet(soms);
        final ArrayList<SelfOrganisingMap> sortedList = new ArrayList();
        while (localSom.size() != 0) {
            SelfOrganisingMap minValue = localSom.iterator().next();
            for (final SelfOrganisingMap som : localSom) {
                if (minValue.getId() > som.getId()) {
                    minValue = som;
                }
            }
            sortedList.add(minValue);
            localSom.remove(minValue);
        }
        return sortedList;
    }

    private Integer findBestMatchingInNodes(final Double[][] nodes) {
        Integer max = nodes[0][0].intValue();
        for (final Double[] row : nodes) {
            for (final Double col : row) {
                if (max < col.intValue()) {
                    max = col.intValue();
                }
            }
        }
        return max;
    }

    private Double[][] mapToUMatrixForNode2DArray(final String term, final Node[][] node) {
        final Double[][] termMatrix = new Double[Constants.NEURONS_DIMENSION][Constants.NEURONS_DIMENSION];
        for (int a = 0; a < node.length; a++) {
            for (int b = 0; b < node[a].length; b++) {
                termMatrix[a][b] = node[a][b].determineDistance(convertWordToIntArray(term));
            }
        }
        return termMatrix;
    }

    private Tuple mapTermToSOM (final Term term, final SelfOrganisingMap som) {
        System.out.println(String.format("[INFO]: Finding Best Matching Node For %s In The Self Organising Map", term.getTerm()));
        double similarityFactor = 0;
        int x = 0;
        int y = 0;
        for (int a = 0; a < som.getSomNetwork().length; a++) {
            for (int b = 0; b < som.getSomNetwork()[a].length; b++) {
                double currentSimilarity = som.getSomNetwork()[a][b]
                        .determineDistance(convertWordToIntArray(term.getTerm()));
                if (currentSimilarity > similarityFactor) {
                    x = a;
                    y = b;
                    similarityFactor = currentSimilarity;
                }
            }
        }
        if (similarityFactor == 0) {
            return randomTermMapping(term, som);
        }
        return bestTermMapping(term, x, y, som);
    }

    private Tuple bestTermMapping(final Term term, int x, int y, final SelfOrganisingMap som) {
        return new Tuple(term.getTerm(), convertIntArrayToWord(som.getSomNetwork()[x][y].getWeights()));
    }

    private Tuple randomTermMapping(final Term term, final SelfOrganisingMap som) {
        final Random random = new Random();
        return new Tuple(term.getTerm(), convertIntArrayToWord(som.getSomNetwork()
                [random.nextInt(Constants.NEURONS_DIMENSION)]
                [random.nextInt(Constants.NEURONS_DIMENSION)].getWeights()));
    }

    private int[] convertWordToIntArray(final String word) {
        final String finalWord = word.replaceAll("\"", "");
        final char[] wordAsCharacters = finalWord.toUpperCase().toCharArray();
        final int[] charsAsIntegers = new int[wordAsCharacters.length];
        for (int a = 0; a < wordAsCharacters.length; a++) {
            charsAsIntegers[a] = (int)wordAsCharacters[a];
        }
        return charsAsIntegers;
    }

    private String convertIntArrayToWord(final int[] asciiArray) {
        String classification = "";
        for (final int asciiValue : asciiArray) {
            classification = classification + Character.toString((char) asciiValue);
        }
        return classification;
    }
}
