package som.domain;

import util.Constants;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Wald_Laptop on 8/9/2017.
 */
public class Node {

    private int[] weights = new int[Constants.WEIGHTS_LENGTH];

    public Node() {}

    public Node(final int[] weights) {
        this.weights = weights;
    }

    public int[] getWeights() {
        return weights;
    }

    public void initializeWeights() {
        final Random random = new Random();
        for (int a = 0; a < weights.length; a++) {
            weights[a] = random.nextInt(
                    (Constants.ASCII_UPPER_BOUND - Constants.ASCII_LOWER_BOUND) +
                            1) + Constants.ASCII_LOWER_BOUND;
        }
    }

    public void updateWeights(final int[] inputValues, final int change) {
        for (int a = 0; a < weights.length; a++) {
            if (a < inputValues.length) {
                if (weights[a] > inputValues[a]) {
                    if (weights[a] - change < inputValues[a])
                        weights[a] = inputValues[a];
                    else
                        weights[a] = weights[a] - change;
                }
                else if (weights[a] < inputValues[a]) {
                    if (weights[a] + change > inputValues[a])
                        weights[a] = inputValues[a];
                    else
                        weights[a] = weights[a] + change;
                }
            }
        }
    }

    public double determineDistance(final int[] wordAsIntArray) {
        double similarityCount = 0;
        for (int a = 0; a < wordAsIntArray.length; a++) {
            if (wordAsIntArray[a] == weights[a]) {
                similarityCount += 1;
            }
        }
        return similarityCount;
    }

    @Override
    public String toString() {
        return "Node{" + "weights=" + Arrays.toString(weights) + '}';
    }
}
