package som.domain;

import org.springframework.stereotype.Component;
import som.Server;
import som.dao.KeywordDao;
import som.db.Keyword;
import util.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Wald_Laptop on 8/8/2017.
 */
@Component
public class SelfOrganisingMap {

    private KeywordDao keywordDao;

    private Node[][] somNetwork = new Node[Constants.NEURONS_DIMENSION][Constants.NEURONS_DIMENSION];

    private final int id;

    private final Node[][] savedSomNetwork;

    public SelfOrganisingMap () {
        this.id = 0;
        this.savedSomNetwork = null;
    }

    public SelfOrganisingMap(final int id, final Node[][] node) {
        this.id = id;
        this.savedSomNetwork = node;
    }

    public int getId() {
        return id;
    }

    public Node[][] getSomNetwork() {
        return somNetwork;
    }

    public void setSomNetwork(Node[][] somNetwork) {
        this.somNetwork = somNetwork;
    }

    public void initializeSOM(final KeywordDao keywordDao) throws IOException{
        this.keywordDao = keywordDao;
        initializeValuesInSOM();
        trainSOM(fetchTrainingTermsForSOM());
    }

    public Node[][] getSavedSomNetwork() {
        return savedSomNetwork;
    }

    private void initializeValuesInSOM() {
        for (int a = 0; a < somNetwork.length; a++) {
            for (int b = 0; b < somNetwork[a].length; b++) {
                final Node node = new Node();
                node.initializeWeights();
                somNetwork[a][b] = node;
                System.out.println(String.format("[INFO]: Initialising node[%s][%s]: %s", a, b, node.toString()));
            }
        }
    }

    private ArrayList<String> fetchTrainingTermsForSOM() throws IOException {
        System.out.println("[INFO]: Fetching Training Words For Self Organising Map");
        final List<Keyword> keywords = keywordDao.findAllKeyword();
        final ArrayList<String> keywordsText = new ArrayList();
        for (final Keyword keyword : keywords) {
            keywordsText.add(keyword.getKeywordValue());
        }
        return keywordsText;
    }

    private void trainSOM(final ArrayList<String> termsToTrainWith) {
        System.out.println("[INFO]: Starting Self Organising Map Training Phase");
        final Random randomizer = new Random();
        double count = 0.0;
        final Node[][][] somNodes = new Node[11][][];
        Server.somAtDifferentIntervals.add(new SelfOrganisingMap(0, fetchValuesAtNode(this)));
        while (count <= 1) {
            final String selectedTerm = termsToTrainWith.get(
                    randomizer.nextInt(termsToTrainWith.size()));
            findBestMatchingNodeAndUpdateNeighbourhood(selectedTerm, 0, 0, 0, findNeighbourhoodDepth(count));
            addAtPosition(count, somNetwork);
            count = count + Constants.SOM_TRAINING_ITERATION_FACTOR;
        }
        Server.somAtDifferentIntervals.add(new SelfOrganisingMap(10, fetchValuesAtNode(this)));
    }

    private void findBestMatchingNodeAndUpdateNeighbourhood(final String selectedTerm, int x, int y,
                                                            double similarityFactor, final int neighbourhoodDepth) {
        System.out.println(String.format("[INFO]: Finding Best Matching Node For %s In The Self Organising Map", selectedTerm));
        for (int a = 0; a < somNetwork.length; a++) {
            for (int b = 0; b < somNetwork[a].length; b++) {
                double currentSimilarity = somNetwork[a][b]
                        .determineDistance(convertWordToIntArray(selectedTerm));
                if (currentSimilarity > similarityFactor) {
                    x = a;
                    y = b;
                    similarityFactor = currentSimilarity;
                }
            }
        }
        if (similarityFactor == 0) {
            final Random random = new Random();
            x = random.nextInt(Constants.NEURONS_DIMENSION);
            y = random.nextInt(Constants.NEURONS_DIMENSION);
        }

        updateNodeAndNeighbours(convertWordToIntArray(selectedTerm), x, y, neighbourhoodDepth);
    }

    private int[] convertWordToIntArray(final String word) {
        final char[] wordAsCharacters = word.toUpperCase().toCharArray();
        final int[] charsAsIntegers = new int[wordAsCharacters.length];
        for (int a = 0; a < wordAsCharacters.length; a++) {
            charsAsIntegers[a] = (int)wordAsCharacters[a];
        }
        return charsAsIntegers;
    }

    private void updateNodeAndNeighbours (final int[] termWeights, final int x, final int y, final int neighbourhoodDepth) {
        System.out.println("[INFO]: Updating Node And Neighbours Weights");
        updateSpecificNode(termWeights, x, y, neighbourhoodDepth);
        for (int a = 1; a < neighbourhoodDepth; a++) {
            if ((x - a >= 0) && (y - a >= 0))
                updateSpecificNode(termWeights, x - a, y - a, neighbourhoodDepth - a + 1);
            if ((x + a <= Constants.NEURONS_DIMENSION - 1) && (y + a <= Constants.NEURONS_DIMENSION - 1))
                updateSpecificNode(termWeights, x + a, y + a, neighbourhoodDepth - a + 1);
            if ((x - a) >= 0)
                updateSpecificNode(termWeights, x - a, y, neighbourhoodDepth - a + 1);
            if ((y - a) >= 0)
                updateSpecificNode(termWeights, x, y - a, neighbourhoodDepth - a + 1);
            if (x + a <= Constants.NEURONS_DIMENSION - 1)
                updateSpecificNode(termWeights, x + a, y, neighbourhoodDepth - a + 1);
            if (y + a <= Constants.NEURONS_DIMENSION - 1)
                updateSpecificNode(termWeights, x, y + a, neighbourhoodDepth - a + 1);
            if ((x - a >= 0) && (y + a <= Constants.NEURONS_DIMENSION - 1))
                updateSpecificNode(termWeights, x - a, y + a, neighbourhoodDepth - a + 1);
            if ((x + a <= Constants.NEURONS_DIMENSION - 1) && (y - a >= 0))
                updateSpecificNode(termWeights, x + a, y - a, neighbourhoodDepth - a + 1);
        }
    }

    private void updateSpecificNode(final int[] termWeights, final int x, final int y, final int factor) {
        somNetwork[x][y].updateWeights(termWeights, factor);
    }

    private int findNeighbourhoodDepth(final double currentIteration) {
        if (currentIteration >= 0.75)
            return 1;
        else if (currentIteration >= 0.50)
            return 2;
        else if (currentIteration >= 0.25)
            return 3;
        else
            return 4;
    }

    private void addAtPosition(final double currentIteration, final Node[][] som) {
        if (currentIteration >= 0.9 && Server.somAtDifferentIntervals.size() == 9)
            Server.somAtDifferentIntervals.add(new SelfOrganisingMap(9, fetchValuesAtNode(this)));
        else if (currentIteration >= 0.8 && Server.somAtDifferentIntervals.size() == 8)
            Server.somAtDifferentIntervals.add(new SelfOrganisingMap(8, fetchValuesAtNode(this)));
        else if (currentIteration >= 0.7 && Server.somAtDifferentIntervals.size() == 7)
            Server.somAtDifferentIntervals.add(new SelfOrganisingMap(7, fetchValuesAtNode(this)));
        else if (currentIteration >= 0.6 && Server.somAtDifferentIntervals.size() == 6)
            Server.somAtDifferentIntervals.add(new SelfOrganisingMap(6, fetchValuesAtNode(this)));
        else if (currentIteration >= 0.5 && Server.somAtDifferentIntervals.size() == 5)
            Server.somAtDifferentIntervals.add(new SelfOrganisingMap(5, fetchValuesAtNode(this)));
        else if (currentIteration >= 0.4 && Server.somAtDifferentIntervals.size() == 4)
            Server.somAtDifferentIntervals.add(new SelfOrganisingMap(4, fetchValuesAtNode(this)));
        else if (currentIteration >= 0.3 && Server.somAtDifferentIntervals.size() == 3)
            Server.somAtDifferentIntervals.add(new SelfOrganisingMap(3, fetchValuesAtNode(this)));
        else if (currentIteration >= 0.2 && Server.somAtDifferentIntervals.size() == 2)
            Server.somAtDifferentIntervals.add(new SelfOrganisingMap(2, fetchValuesAtNode(this)));
        else if (currentIteration >= 0.1 && Server.somAtDifferentIntervals.size() == 1)
            Server.somAtDifferentIntervals.add(new SelfOrganisingMap(1, fetchValuesAtNode(this)));
    }

    private Node[][] fetchValuesAtNode(final SelfOrganisingMap map) {
        final Node[][] nodes = new Node[Constants.NEURONS_DIMENSION][Constants.NEURONS_DIMENSION];
        for (int a = 0; a < nodes.length; a++) {
            for (int b = 0; b < nodes.length; b++) {
                final int[] weights = new int[Constants.WEIGHTS_LENGTH];
                for (int c = 0; c < weights.length; c++) {
                    weights[c] = map.getSomNetwork()[a][b].getWeights()[c];
                }
                nodes[a][b] = new Node(weights);
            }
        }
        return nodes;
    }
}