package chatbot.domain;

/**
 * Created by Wald_Laptop on 10/14/2017.
 */
public class Constants {

    public static final String[] DEFAULT        = {"default"};
    public static final String[] HELLO          = {"hello", "hi"};
    public static final String[] ANALYSIS       = {"analysis", "analyze", "classification", "matching"};
    public static final String[] TDM            = {"tdm", "term", "document", "matrix"};
    public static final String[] SOM            = {"som", "self", "organising", "map"};
    public static final String[] INFO           = {"info", "help", "assist"};
    public static final String[] NO_MATCH       = {"no match"};
    public static final String[] MXM            = {"mxm"};
    public static final String[] WBJ            = {"waar", "bly", "jy"};
    public static final String[] WAY            = {"where"};
}
