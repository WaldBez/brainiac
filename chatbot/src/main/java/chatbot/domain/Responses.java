package chatbot.domain;

import java.util.HashMap;

/**
 * Created by Wald_Laptop on 10/14/2017.
 */
public class Responses extends Constants {

    private HashMap<String[], String> responses;

    public Responses() {
        responses = new HashMap();
        responses.put(DEFAULT, "Hi, my name is Flappity Waddlebottom and I am your friendly Brainiac Assistant.");
        responses.put(HELLO, "How may I help you today?");
        responses.put(ANALYSIS, "In order to do analysis, we first convert your documents to a Term Document Matrix(TDM), and then we run this TDM in a Self-Organising Map(SOM) which is trained from a flattened ontology, which you submit. Based on this we classify your documents and display them.");
        responses.put(TDM, "The term document matrix is a matrix we create from the documents you submit to the system. This matrix sets the document name and terms in the documents as the heading row and column. Where these two values intersect in the matrix, we store the amount of times that term occurred in the document.");
        responses.put(SOM, "The self-organising map takes each term in the term document matrix and classifies them. The SOM has two phases. In the first phase the SOM is trained where the nodes are moved closes to the training terms. The second phase classifies terms submitted to the SOM.");
        responses.put(INFO, "Feel free to ask me about self-organising maps(som), term document matrices(tdm) or our analysis process. Alternatively you can try find some funny easter eggs programmed into me.");
        responses.put(NO_MATCH, "What you talkin about Willis?");
        responses.put(MXM, "Why you such a MXM?");
        responses.put(WBJ, "Jy Bly stil!!");
        responses.put(WAY, "We OUT CHEA!");
    }

    public HashMap<String[], String> getResponses() {
        return responses;
    }
}
