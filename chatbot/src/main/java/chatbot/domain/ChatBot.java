package chatbot.domain;

/**
 * Created by Wald_Laptop on 10/29/2017.
 */
public class ChatBot extends Constants {
    private String[][] keywords = {DEFAULT, HELLO, ANALYSIS, TDM, SOM, INFO, NO_MATCH, MXM, WBJ, WAY};
    final Responses responses = new Responses();

    public ChatBot() {}

    public String respond(final String question) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return findBestMatchingResponse(question);
    }

    private String findBestMatchingResponse(final String question) {
        final String[] textAsWords = question.split("\\W");
        String[] keyword = null;
        for (int a = 0; a < textAsWords.length; a++) {
            keyword = wordMatches(textAsWords[a]);
            if (!keyword.equals(NO_MATCH))
                break;
        }
        return responses.getResponses().get(keyword);
    }

    private String[] wordMatches(final String word) {
        final char[] wordAsChars = word.toLowerCase().toCharArray();
        int similarity = 0;
        for (final String[] keywordArr : keywords) {
            for (final String keyword : keywordArr) {
                final char[] keywordAsChars = keyword.toCharArray();
                for (int a = 0; a < wordAsChars.length; a++) {
                    if (keywordAsChars.length > a && keywordAsChars[a] == wordAsChars[a]) {
                        similarity += 1;
                    }
                }
                if ((similarity / keywordAsChars.length) >= 0.65) {
                    return keywordArr;
                }
                similarity = 0;
            }
        }
        return NO_MATCH;
    }
}
