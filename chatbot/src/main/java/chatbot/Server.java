package chatbot;

import chatbot.domain.ChatBot;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * Created by Wald_Laptop on 10/29/2017.
 */
@Component
@RestController
public class Server {

    private ChatBot chatBot;

    @PostConstruct()
    public void initializeChatBot(){
        chatBot = new ChatBot();
    }

    @RequestMapping(value = "/lets/talk", method = RequestMethod.POST)
    public String letsTalk(@RequestBody final String question) throws IOException {
        return chatBot.respond(question);
    }
}
