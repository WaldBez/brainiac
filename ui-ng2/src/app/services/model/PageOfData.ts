/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface PageOfData {
    classification?: models.Classification;

    contentData?: Array<string>;

    document?: models.Document;

    effectiveFrom?: Date;

    effectiveTo?: Date;

    links?: Array<string>;

    pageId?: number;

}
