import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class BotService {

  response: string = '';

  constructor(private http: Http) { }

  public sendMessage(message: string) {
    return this.http.post('http://localhost:8000/chatbot/talk', message);
  }
}
