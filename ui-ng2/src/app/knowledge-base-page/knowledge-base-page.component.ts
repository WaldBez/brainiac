import { Component, OnInit } from '@angular/core';
import {Page} from "../services/model/Page";
import {AnalysiscontrollerApi} from "../services/api/AnalysiscontrollerApi";

@Component({
  selector: 'knowledge-base-page',
  templateUrl: './knowledge-base-page.component.html',
  styleUrls: ['./knowledge-base-page.component.css']
})
export class KnowledgeBasePageComponent implements OnInit {

  pages: Page[] = [];

  constructor(private analysisApi: AnalysiscontrollerApi) {}

  ngOnInit() {}

  runAnalysis() {
    this.analysisApi.runFullAnalysisUsingGET()
      .subscribe((pages: Page[]) => this.pages = pages);
  }
}
