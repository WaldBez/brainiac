import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {FilesRepositoryComponent} from "./files-repository/files-repository.component";
import {UsersPageComponent} from './users-page/users-page.component';
import {KnowledgeBasePageComponent} from './knowledge-base-page/knowledge-base-page.component';
import {KeywordsPageComponent} from './keywords-page/keywords-page.component';
import {AnalysiscontrollerApi} from './services/api/AnalysiscontrollerApi'
import {FilecontrollerApi} from './services/api/FilecontrollerApi'
import {PerfectScrollbarModule} from 'angular2-perfect-scrollbar';
import {PerfectScrollbarConfigInterface} from 'angular2-perfect-scrollbar';
import {RouterModule, Routes} from '@angular/router';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';
import {KeywordcontrollerApi} from "./services/api/KeywordcontrollerApi";
import {BotService} from "./services/api/bot.service";

const routes: Routes = [
  {path: 'files', component: FilesRepositoryComponent},
  {path: 'keywords', component: KeywordsPageComponent},
  {path: 'users', component: UsersPageComponent},
  {path: 'knowledgebase', component: KnowledgeBasePageComponent}
];

const PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    FilesRepositoryComponent,
    UsersPageComponent,
    KnowledgeBasePageComponent,
    KeywordsPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    PerfectScrollbarModule.forRoot(PERFECT_SCROLLBAR_CONFIG),
    Ng2GoogleChartsModule
  ],
  providers: [AnalysiscontrollerApi, FilecontrollerApi, KeywordcontrollerApi, BotService],
  bootstrap: [AppComponent]
})
export class AppModule { }
