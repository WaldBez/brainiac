import {Component, OnInit, ViewChild} from '@angular/core';
import {AnalysiscontrollerApi} from "../services/api/AnalysiscontrollerApi";

@Component({
  selector: 'users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.css']
})
export class UsersPageComponent implements OnInit {


  @ViewChild('uMatrix') uMatrixVC;

  uMatrixTerm: string = 'Computer';
  uMatrixData: Array<Array<number>>;
  uMatrix =  {
    chartType: 'BubbleChart',
    dataTable: [],
    options: {title: 'U-Matrix', height: 700, width: 1000},
  };

  @ViewChild('trainingChart') trainingChartVC;
  trainingChartData: Array<number>;
  trainingChart = {
    chartType: 'AreaChart',
    dataTable: [],
    options: {title: 'Best Matching Nodes Over Time', height: 700, width: 1000},
  };

  constructor(private analysisApi: AnalysiscontrollerApi) { }

  ngOnInit() {
    this.fetchUMatrix(false);
    this.fetchAreaChart(false);
  }

  submitWordToUMatrix(redraw: boolean) {
    this.fetchUMatrix(redraw);
    this.fetchAreaChart(redraw);
  }

  fetchUMatrix(redraw: boolean) {
    this.analysisApi.fetchUMatrixUsingPOST(this.uMatrixTerm)
      .subscribe(response => {
        this.uMatrixData = response;
        this.uMatrix =  {
          chartType: 'BubbleChart',
          dataTable: this.covertToDataTable(),
          options: {title: 'U-Matrix: ' + this.uMatrixTerm, height: 700, width: 1000},
        };
        if (redraw) {
          this.uMatrixVC.redraw();
        }
      });
  }

  fetchAreaChart(redraw: boolean) {
    this.analysisApi.fetchSomDistanceAtDifferentTrainingPointsUsingPOST(this.uMatrixTerm)
      .subscribe(response => {
        this.trainingChartData = response;
        this.trainingChart = {
          chartType: 'AreaChart',
          dataTable: this.covertToAreaChart(),
          options: {title: 'Best Matching Nodes Over Time: ' + this.uMatrixTerm, height: 700, width: 1000},
        }
        if (redraw) {
          this.uMatrixVC.redraw();
        }
      });
  }

  covertToDataTable() {
    var data = [];
    data.push(['Node ' + this.uMatrixTerm, 'X', 'Y', 'Distance', 'Distance']);
    for (var a = 0; a < this.uMatrixData.length; a++) {
      for (var b = 0; b < this.uMatrixData[a].length; b++) {
        data.push(["", a + 1, b + 1, this.uMatrixData[a][b], this.uMatrixData[a][b]]);
      }
    }
    return data;
  }

  covertToAreaChart() {
    var data = [];
    var percentage = 0;
    data.push(['Percentage Completed', 'Best Node'])
    for (var a = 0; a < this.trainingChartData.length; a++) {
      data.push([percentage + "%", this.trainingChartData[a]]);
      percentage = percentage + 10;
    }
    return data;
  }
}
