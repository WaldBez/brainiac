import { Component, OnInit } from '@angular/core';
import {BotService} from "../services/api/bot.service";

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  messages: Array<String> = [];
  currentQuestion: string = '';
  sideBarIsHidden: boolean = false;

  constructor(private chatBot: BotService) { }

  ngOnInit() {
    this.chatBot.sendMessage('default')
      .subscribe (response => {
        this.messages.push(response['_body']);
      });
  }

  hideSidebar() {
    this.sideBarIsHidden = true;
  }

  showSidebar() {
    this.sideBarIsHidden = false;
  }

  sendMessage() {
    if (this.currentQuestion !== '') {
      this.messages.push(this.currentQuestion);
      this.chatBot.sendMessage(this.currentQuestion).subscribe (response => {
        this.messages.push(response['_body']);
      });
      this.currentQuestion = '';
    }
  }
}
