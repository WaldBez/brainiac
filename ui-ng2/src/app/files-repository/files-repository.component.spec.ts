import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilesRepositoryComponent } from './files-repository.component';

describe('FilesRepositoryComponent', () => {
  let component: FilesRepositoryComponent;
  let fixture: ComponentFixture<FilesRepositoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilesRepositoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilesRepositoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
