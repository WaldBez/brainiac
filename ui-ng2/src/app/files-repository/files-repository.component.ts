import { Component, OnInit } from '@angular/core';
import {FilecontrollerApi} from "../services/api/FilecontrollerApi";
import {DocumentModel} from "../services/model/DocumentModel";

@Component({
  selector: 'files-repository',
  templateUrl: './files-repository.component.html',
  styleUrls: ['./files-repository.component.css']
})
export class FilesRepositoryComponent implements OnInit {

  documents: DocumentModel[] = [];
  response: String = "";

  constructor(private fileApi: FilecontrollerApi) { }

  ngOnInit() {
    this.fileApi.fetchAllDocumentsUsingGET()
      .subscribe((response: Array<DocumentModel>) => this.documents = response);
  }

  saveFilesToServer() {
    this.fileApi.receiveFilesUsingPOST(this.documents)
      .subscribe((response: String) => this.response = response);
    location.reload();
  }

  deleteDocument(document: DocumentModel) {
    this.fileApi.deleteDocumentsUsingPOST(document)
      .subscribe((respone: String) => {
        this.response = respone;
      });
    location.reload();
  }

  fileChanged($event):void {
    var doc: DocumentModel = {};
    var file = (<HTMLInputElement>document.getElementById("file")).files[0];
    var fileReader = new FileReader();
    fileReader.readAsText(file, "UTF-8");
    fileReader.onload = (evt: FileReaderEvent) => {
      doc.fileAsText = evt.target.result;
      doc.name = file.name;
      doc.analyse = false;
      this.documents.push(doc);
    }
  }
}

interface FileReaderEventTarget extends EventTarget {
  result:string
}

interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage():string;
}
