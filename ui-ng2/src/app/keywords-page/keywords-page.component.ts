import {Component, OnInit} from '@angular/core';
import {Keyword} from "../services/model/Keyword";
import {KeywordcontrollerApi} from "../services/api/KeywordcontrollerApi";
import {DocumentModel} from "../services/model/DocumentModel";

@Component({
  selector: 'keywords-page',
  templateUrl: './keywords-page.component.html',
  styleUrls: ['./keywords-page.component.css'],
  providers: [KeywordcontrollerApi]
})
export class KeywordsPageComponent implements OnInit {

  keywords: Keyword[] = [];
  response: String = "";

  constructor(private keywordController: KeywordcontrollerApi) {}

  ngOnInit() {
    this.keywordController.fetchAllKeywordsUsingGET()
      .subscribe((response: Array<Keyword>) => this.keywords = response);
  }

  saveKeywordsToServer() {
    this.keywordController.saveKeywordsUsingPOST(this.keywords)
      .subscribe((response: String) => this.response = response);
    location.reload();
  }

  addKeyword() {
    var keyword: Keyword = {};
    keyword.analyseWord = false;
    this.keywords.push(keyword);
  }

  retrainSOM() {
    this.keywordController.retrainSOMUsingGET()
      .subscribe((respone: String) => {
      this.response = respone;
    });
    location.reload();
  }

  fileChanged($event):void {
    var doc: DocumentModel = {};
    var file = (<HTMLInputElement>document.getElementById("file")).files[0];
    var fileReader = new FileReader();
    fileReader.readAsText(file, "UTF-8");
    fileReader.onload = (evt: FileReaderEvent) => {
      doc.fileAsText = evt.target.result;
      doc.name = file.name;
      doc.analyse = false;
      this.keywordController.saveNewDomainUsingPOST(doc)
        .subscribe((response: String) => {
          this.response = response;
        });
    }
    location.reload();
  }

  deleteKeyword(keyword: Keyword) {
    this.keywordController.deleteKeywordUsingPOST(keyword)
      .subscribe((respone: String) => {
        this.response = respone;
      });
    location.reload();
  }
}

interface FileReaderEventTarget extends EventTarget {
  result:string
}

interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage():string;
}
