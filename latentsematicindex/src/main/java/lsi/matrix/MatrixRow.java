package lsi.matrix;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Wald_Laptop on 5/7/2017.
 */
public class MatrixRow {

    private final String documentNameAsId;
    private final HashMap<String, Integer> termsCount = new HashMap<String, Integer>();

    public MatrixRow (final String documentNameAsId) {
        this.documentNameAsId = documentNameAsId;
    }

    public void increaseCountValue (final String key) {
        termsCount.put(key, termsCount.get(key) + 1);
    }

    public String getDocumentNameAsId() {
        return documentNameAsId;
    }

    public HashMap<String, Integer> getTermsCount() {
        return termsCount;
    }

    public void printRow () {
        System.out.print(documentNameAsId + " | ");
        final Iterator iterator = termsCount.entrySet().iterator();
        while (iterator.hasNext()) {
            final HashMap.Entry entry = (HashMap.Entry) iterator.next();
            System.out.print(entry.getKey() + ": " + entry.getValue() + " | ");
        }
        System.out.println();
    }
}