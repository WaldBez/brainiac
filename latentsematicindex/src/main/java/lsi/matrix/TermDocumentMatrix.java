package lsi.matrix;

import lsi.processors.TextDocumentTuple;

import java.util.ArrayList;

/**
 * Created by Wald_Laptop on 5/7/2017.
 */
public class TermDocumentMatrix {

    final ArrayList<MatrixRow> rows = new ArrayList();
    ArrayList<TextDocumentTuple> paragraphs = new ArrayList();

    public void addItem(final MatrixRow row) {
        rows.add(row);
    }

    public void changeRowValue(final int index, final MatrixRow row) {
        rows.set(index, row);
    }

    public void changeSingleValueInRow(final int rowIndex, final String key) {
        rows.get(rowIndex).increaseCountValue(key);
    }

    public ArrayList<TextDocumentTuple> getParagraphs() {
        return paragraphs;
    }

    public void setParagraphs(final ArrayList<TextDocumentTuple> paragraphs) {
        this.paragraphs = paragraphs;
    }

    public ArrayList<MatrixRow> getRows() {
        return rows;
    }

    public void printMatrix () {
        for (final MatrixRow row : rows) {
            row.printRow();
        }
    }
}