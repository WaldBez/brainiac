package lsi.matrix;

import lsi.processors.TextDocumentTuple;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Wald_Laptop on 5/7/2017.
 */
public class MatrixConstructor {

    public static TermDocumentMatrix createMatrix (final ArrayList<TextDocumentTuple> tuples) {
        final TermDocumentMatrix matrix = new TermDocumentMatrix();
        final Set<String> allUniqueWords = getAllUniqueWordsInText(tuples);
        for (final TextDocumentTuple tuple : tuples) {
            matrix.addItem(createRow(tuple, allUniqueWords));
        }
        matrix.setParagraphs(tuples);
        return matrix;
    }

    private static Set<String> getAllUniqueWordsInText (final ArrayList<TextDocumentTuple> tuples) {
        final Set<String> uniqueWords = new HashSet();
        for (final TextDocumentTuple tuple : tuples) {
            uniqueWords.addAll(tuple.getTextAsWords());
        }
        return uniqueWords;
    }

    private static MatrixRow createRow (final TextDocumentTuple tuple, final Set<String> allUniqueWords) {
        final MatrixRow row = new MatrixRow(tuple.getDocument());
        for (final String word : allUniqueWords) {
            row.getTermsCount().put(word, 0);
        }
        increaseWordValues(tuple, row);
        return row;
    }

    private static void increaseWordValues (final TextDocumentTuple tuple, final MatrixRow row) {
        for (final String word : tuple.getTextAsWords()) {
            row.increaseCountValue(word);
        }
    }
}
