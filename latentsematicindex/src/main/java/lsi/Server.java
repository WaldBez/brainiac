package lsi;

import lsi.matrix.MatrixConstructor;
import lsi.matrix.TermDocumentMatrix;
import lsi.util.TuplesFileUtil;
import models.DocumentModel;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Wald on 3/5/2017.
 */
@RestController
public class Server {

    @RequestMapping("/")
    public String home() throws IOException {
        return "Welcome To Latent Semantic Index Server.";
    }

    @RequestMapping(value = "/RunLSI", method = RequestMethod.POST)
    public TermDocumentMatrix createTDM(@RequestBody final ArrayList<DocumentModel> documentModels) throws IOException {
        final TuplesFileUtil handler = new TuplesFileUtil();
        final TermDocumentMatrix matrix =
                MatrixConstructor.createMatrix(handler.createTuplesFromFiles(documentModels));
        matrix.printMatrix();
        return matrix;
    }
}
