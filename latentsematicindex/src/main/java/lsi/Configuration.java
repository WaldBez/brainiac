package lsi;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by Wald on 3/5/2017.
 */
@ImportResource("classpath:spring/spring-context.xml")
@EnableConfigurationProperties
public class Configuration {
}
