package lsi.util;

import lsi.processors.TextDocumentTuple;
import models.DocumentModel;
import util.FileHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Wald_Laptop on 8/12/2017.
 */
public class TuplesFileUtil {

    final FileHandler fileHandler = new FileHandler();

    public ArrayList<TextDocumentTuple> createTuplesFromFiles(final ArrayList<DocumentModel> documentModels) throws IOException {
        final ArrayList<TextDocumentTuple> tuples = new ArrayList<TextDocumentTuple>();
        for(final DocumentModel documentModel : documentModels) {
            tuples.addAll(createTupleFromStringText(documentModel));
        }
        return tuples;
    }

    private ArrayList<TextDocumentTuple> createTuples(final DocumentModel documentModel) throws IOException {
        final ArrayList<TextDocumentTuple> tuples = new ArrayList<TextDocumentTuple>();
        final BufferedReader reader = fileHandler.createBufferedReaderForFile(documentModel);
        final ArrayList<String> paragraphs = fileHandler.createStringOfAllLinesInFile(reader);
        for (int a = 0; a < paragraphs.size(); a++) {
            tuples.add(createTuple(documentModel, a, paragraphs.get(a)));
        }
        return tuples;
    }

    private ArrayList<TextDocumentTuple> createTupleFromStringText(final DocumentModel documentModel) {
        final ArrayList<TextDocumentTuple> tuples = new ArrayList<TextDocumentTuple>();
        final ArrayList<String> paragraphs = new ArrayList<String>(Arrays.asList(documentModel.getFileAsText().split("\n\r")));
        for (int a = 0; a < paragraphs.size(); a++) {
            tuples.add(createTuple(documentModel, a, paragraphs.get(a)));
        }
        return tuples;

    }

    private TextDocumentTuple createTuple(final DocumentModel documentModel, final int paragraphIndex, final String paragraph) {
        final TextDocumentTuple tuple = new TextDocumentTuple(documentModel.getName() + "p_" + paragraphIndex);
        tuple.setTextUsingDocumentsText(paragraph);
        return tuple;
    }
}
