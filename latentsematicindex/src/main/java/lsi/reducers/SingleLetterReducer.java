package lsi.reducers;

import util.Constants;
import util.FileHandler;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Wald on 3/15/2017.
 */
public class SingleLetterReducer implements LanguageReducer {

    private ArrayList<String> letters = new ArrayList<String>();

    public SingleLetterReducer() throws IOException {
        final FileHandler letterFileHandler = new FileHandler();
        letters = letterFileHandler.readWordsFromFile(Constants.SINGLE_LETTERS_FILE);
    }

    public boolean removeWord(final String word) {
        if (letters.contains(word.toLowerCase())) {
            return true;
        }
        return false;
    }
}