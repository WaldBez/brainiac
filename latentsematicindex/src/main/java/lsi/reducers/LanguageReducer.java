package lsi.reducers;

/**
 * Created by Wald on 3/12/2017.
 */
public interface LanguageReducer {

    boolean removeWord(final String word);
}
