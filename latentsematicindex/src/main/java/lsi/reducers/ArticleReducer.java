package lsi.reducers;

import util.Constants;
import util.FileHandler;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Wald on 3/12/2017.
 */
public class ArticleReducer implements LanguageReducer {

    private ArrayList<String> articles = new ArrayList<String>();

    public ArticleReducer() throws IOException {
        final FileHandler articleFileHandler = new FileHandler();
        articles = articleFileHandler.readWordsFromFile(Constants.ARTICLES_FILE);
    }

    public boolean removeWord(final String word) {
        if (articles.contains(word.toLowerCase())) {
            return true;
        }
        return false;
    }
}
