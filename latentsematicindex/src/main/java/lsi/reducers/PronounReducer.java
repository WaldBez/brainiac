package lsi.reducers;

import util.Constants;
import util.FileHandler;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Wald on 3/15/2017.
 */
public class PronounReducer implements LanguageReducer {

    private ArrayList<String> pronouns = new ArrayList<String>();

    public PronounReducer() throws IOException {
        final FileHandler pronounFileHandler = new FileHandler();
        pronouns = pronounFileHandler.readWordsFromFile(Constants.PRONOUNS_FILE);
    }

    public boolean removeWord(final String word) {
        if (pronouns.contains(word.toLowerCase())) {
            return true;
        }
        return false;
    }
}