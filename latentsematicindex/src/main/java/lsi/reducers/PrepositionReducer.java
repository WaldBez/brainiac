package lsi.reducers;

import util.Constants;
import util.FileHandler;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Wald on 3/14/2017.
 */
public class PrepositionReducer implements LanguageReducer {

    private ArrayList<String> prepositions = new ArrayList<String>();

    public PrepositionReducer() throws IOException {
        final FileHandler prepositionFileHandler = new FileHandler();
        prepositions = prepositionFileHandler.readWordsFromFile(Constants.PREPOSITIONS_FILE);
    }

    public boolean removeWord(final String word) {
        if (prepositions.contains(word.toLowerCase())) {
            return true;
        }
        return false;
    }
}
