package lsi.reducers;

import util.Constants;
import util.FileHandler;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Wald on 3/15/2017.
 */
public class FrillyWordsReducer implements LanguageReducer {

    private ArrayList<String> frillyWords = new ArrayList<String>();

    public FrillyWordsReducer() throws IOException {
        final FileHandler frillyWordFileHandler = new FileHandler();
        frillyWords = frillyWordFileHandler.readWordsFromFile(Constants.FRILLY_WORDS_FILE);
    }

    public boolean removeWord(final String word) {
        if (frillyWords.contains(word.toLowerCase())) {
            return true;
        }
        return false;
    }
}