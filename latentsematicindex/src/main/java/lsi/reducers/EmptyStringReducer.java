package lsi.reducers;

/**
 * Created by Wald on 3/15/2017.
 */
public class EmptyStringReducer implements LanguageReducer {

    public boolean removeWord(String word) {
        if (word.equals("")) {
            return true;
        }
        return false;
    }
}
