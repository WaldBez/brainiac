package lsi.reducers;

import util.Constants;
import util.FileHandler;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Wald on 3/14/2017.
 */
public class ConjunctionReducer implements LanguageReducer {

    private ArrayList<String> conjunctions = new ArrayList<String>();

    public ConjunctionReducer() throws IOException {
        final FileHandler conjunctionFileHandler = new FileHandler();
        conjunctions = conjunctionFileHandler.readWordsFromFile(Constants.CONJUNCTIONS_FILE);
    }

    public boolean removeWord(final String word) {
        if (conjunctions.contains(word.toLowerCase())) {
            return true;
        }
        return false;
    }
}