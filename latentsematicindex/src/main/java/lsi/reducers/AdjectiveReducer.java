package lsi.reducers;

import util.Constants;
import util.FileHandler;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Wald on 3/15/2017.
 */
public class AdjectiveReducer implements LanguageReducer {

    private ArrayList<String> adjectives = new ArrayList<String>();

    public AdjectiveReducer() throws IOException {
        final FileHandler adjectiveFileHandler = new FileHandler();
        adjectives = adjectiveFileHandler.readWordsFromFile(Constants.ADJECTIVES_FILE);
    }

    public boolean removeWord(final String word) {
        if (adjectives.contains(word.toLowerCase())) {
            return true;
        }
        return false;
    }
}
