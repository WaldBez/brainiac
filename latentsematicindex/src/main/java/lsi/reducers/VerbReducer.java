package lsi.reducers;

import util.Constants;
import util.FileHandler;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Wald on 3/15/2017.
 */
public class VerbReducer implements LanguageReducer {

    private ArrayList<String> verbs = new ArrayList<String>();

    public VerbReducer() throws IOException {
        final FileHandler verbFileHandler = new FileHandler();
        verbs = verbFileHandler.readWordsFromFile(Constants.VERBS_FILE);
    }

    public boolean removeWord(final String word) {
        if (verbs.contains(word.toLowerCase())) {
            return true;
        }
        return false;
    }
}