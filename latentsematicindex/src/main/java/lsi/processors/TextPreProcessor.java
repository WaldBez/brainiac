package lsi.processors;

import lsi.reducers.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Wald on 3/12/2017.
 */
public class TextPreProcessor {

    final static String REGEX_TO_SPLIT_TEXT_WITH = "\\W";
    final AdjectiveReducer adjectiveReducer;
    final ArticleReducer articleReducer;
    final ConjunctionReducer conjunctionReducer;
    final EmptyStringReducer emptyStringReducer;
    final FrillyWordsReducer frillyWordsReducer;
    final PrepositionReducer prepositionReducer;
    final PronounReducer pronounReducer;
    final SingleLetterReducer singleLetterReducer;
    final VerbReducer verbReducer;

    public TextPreProcessor() throws IOException {
        adjectiveReducer = new AdjectiveReducer();
        articleReducer = new ArticleReducer();
        conjunctionReducer = new ConjunctionReducer();
        emptyStringReducer = new EmptyStringReducer();
        frillyWordsReducer = new FrillyWordsReducer();
        prepositionReducer = new PrepositionReducer();
        pronounReducer = new PronounReducer();
        singleLetterReducer = new SingleLetterReducer();
        verbReducer = new VerbReducer();
    }

    public ArrayList<String> reduceText(final String text) {
        final String[] wordsFromTheText = text.split(REGEX_TO_SPLIT_TEXT_WITH);
        final ArrayList<String> wordsToKeep = new ArrayList<String>();
        for (int a = 0; a < wordsFromTheText.length; a++) {
            determineIfWordNeedsToBeKept(wordsFromTheText[a], wordsToKeep);
        }
        return wordsToKeep;
    }

    private void determineIfWordNeedsToBeKept(final String currentWord, final ArrayList<String> wordsToKeep) {
        if (!adjectiveReducer.removeWord(currentWord) && !articleReducer.removeWord(currentWord) &&
                !conjunctionReducer.removeWord(currentWord) && !emptyStringReducer.removeWord(currentWord) &&
                !frillyWordsReducer.removeWord(currentWord) && !prepositionReducer.removeWord(currentWord) &&
                !singleLetterReducer.removeWord(currentWord) &&
                !pronounReducer.removeWord(currentWord) && !verbReducer.removeWord(currentWord)) {
            wordsToKeep.add(currentWord);
        }
    }
}
