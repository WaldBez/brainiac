package lsi.processors;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Wald_Laptop on 5/7/2017.
 */
public class TextDocumentTuple {

    private final String document;
    private String text;
    private ArrayList<String> textAsWords = new ArrayList<String>();

    public TextDocumentTuple(final String document) {
        this.document = document;
    }

    public void setTextUsingDocumentsText(final String text) {
        try {
            this.text = text;
            final TextPreProcessor preProcessor = new TextPreProcessor();
            textAsWords = preProcessor.reduceText(text);
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    public String getDocument() {
        return document;
    }

    public String getText() {
        return text;
    }

    public ArrayList<String> getTextAsWords() {
        return listAsUpperCase();
    }

    public void setTextAsWords(ArrayList<String> textAsWords) {
        this.textAsWords = textAsWords;
    }

    private ArrayList<String> listAsUpperCase() {
        final ArrayList<String> upperCaseWords = new ArrayList();
        for (final String word : textAsWords) {
            upperCaseWords.add(word.toUpperCase());
        }
        return upperCaseWords;
    }
}
